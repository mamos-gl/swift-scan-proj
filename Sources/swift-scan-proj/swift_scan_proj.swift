public struct swift_scan_proj {
    public private(set) var text = "Hello, World!"

    public init() {
    }
    
    public func greet(name: String) -> String {
        return "Hello, \(name)!"
    }
}