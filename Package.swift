// swift-tools-version: 5.10
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "swift-scan-proj",
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "swift-scan-proj",
            targets: ["swift-scan-proj"]),
    ],
    dependencies: [
        .package(url: "https://github.com/Alamofire/Alamofire.git", from: "5.4.0"),
        .package(url: "https://github.com/vapor/postgres-nio.git", from: "1.0.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: "swift-scan-proj",
            dependencies: [
                .product(name: "PostgresNIO", package: "postgres-nio")
            ]),
        .testTarget(
            name: "swift-scan-projTests",
            dependencies: ["swift-scan-proj"]),
    ]
)
