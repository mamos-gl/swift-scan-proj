import XCTest
@testable import swift_scan_proj

final class swift_scan_projTests: XCTestCase {
     func testGreet() {
        let library = MyLibrary()
        XCTAssertEqual(library.greet(name: "Swift"), "Hello, Swift!")
    }
}
