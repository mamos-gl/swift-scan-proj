# swift-spm

Test project with:

* **Language:** Swift
* **Package Manager:** SPM
* **Framework:** Server Side

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use-a-test-project) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported                 |
|---------------------|---------------------------|
| SAST                |  :x: |
| Dependency Scanning | :white_check_mark: |
| Container Scanning  |  :x: |
| DAST                |  :x: |
| License Management  | :x: |
